<?php

namespace RunTests;

require_once('vendor/autoload.php');

class RunTests
{

	private $totalAssertions = 0;
	
	private $totalTests = 0;
	
	private $totalFiles = 0;
	
	private $totalErrors = 0;
	
	private $totalIncomplete = 0;
	
	private $totalSkipped = 0;
	
	private $db = null;
	
	private $dbVersion = 7;
	
	private $showSkipped = false;
	
	private $ini = array();
	
	private $width = 60;
	
	public function __construct()
	{
		$this->db = new \SQLite3(sys_get_temp_dir() . "/db.{$this->dbVersion}.sqlite3");
		$this->createDbTables();
		
		// read INI file
		if(!file_exists('run-tests.ini')) {
			echo "Missing run-tests.ini. You may create it from the sample:\n\n";
			echo "  cp vendor/chancey/run-tests/run-tests.sample.ini run-tests.ini\n\n";
			exit(1);
		}
		$this->ini = parse_ini_file('run-tests.ini', true);

        if (!isset($this->ini['phpunit']['exe'])) {
        	die("Fatal: Please configure phpunit in run-tests (see run-tests.sample.ini)\n\n");
        }
        /*if (!isset($this->ini['doxygen']['exe'])) {
        	die("Fatal: Please configure doxygen in run-tests (see run-tests.sample.ini)\n\n");
        }*/
		
		// generate real paths
		$this->buildRealPaths($this->ini['source']['include']);
		$this->buildRealPaths($this->ini['source']['exclude']);
		$this->buildRealPaths($this->ini['test']['include']);
		$this->buildRealPaths($this->ini['test']['exclude']);
	}
	
	public static function buildRealPaths(&$paths)
	{
		if(!is_array($paths)) {
			return;
		}
		
		foreach($paths as &$path) {
			if(!realpath($path)) {
				die("Cannot resolve real path of '$path'\n");
			}
			$path = realpath($path);
		}
	}
	
	protected function createDbTables()
	{
		@$this->db->exec('CREATE TABLE test (test_file TEXT, last_run INT, modified TEXT, total_tests INT, total_assertions INT, total_incomplete INT, total_errors INT, total_skipped INT, errors TEXT, PRIMARY KEY (test_file))');
		@$this->db->exec('CREATE TABLE test_file (test_file TEXT, source_file TEXT, modified TEXT, PRIMARY KEY (test_file, source_file))');
		@$this->db->exec('CREATE TABLE doxygen (doxygen_id INT, file TEXT, line INT, message TEXT, modified_time TEXT, PRIMARY KEY (doxygen_id))');
	}
	
	public function run()
	{
        $this->cleanDeletedFiles();
        
		foreach($this->ini['test']['include'] as $dir) {
			$this->runTestsForPath($dir);
		}
		$this->printFinalStats();
		
        // doxygen last
		//$this->runDoxygen();
	}
	
	protected function isTestFile($path)
	{
		foreach($this->ini['test']['testPatterns'] as $pattern) {
			if(preg_match($pattern, $path)) {
				return true;
			}
		}
		return false;
	}
	
	protected function isSourceFile($path)
	{
		foreach($this->ini['source']['include'] as $dir) {
			if(self::startsWith($path, $dir)) {
				return true;
			}
		}
		return false;
	}
	
	protected function runTestsForPath($path)
	{
		// found a test?
		if(!is_dir($path)) {
			// check if this is a test file
			if(!$this->isTestFile($path)) {
				return;
			}
			
			$this->runSingleTest($path);
			return;
		}
	
		// read the include folder
		$files = scandir($path);
		foreach($files as $file) {
			if(substr($file, 0, 1) == '.') {
				continue;
			}
			$this->runTestsForPath("$path/$file");
		}
	}
	
	protected function getRelativeSourcePath($path)
	{
		foreach($this->ini['source']['include'] as $dir) {
			if(self::startsWith($path, $dir)) {
				return substr($path, strlen($dir) + 1);
			}
		}
		return null;
	}
	
	protected function getRelativeTestPath($path)
	{
		foreach($this->ini['test']['include'] as $dir) {
			if(self::startsWith($path, $dir)) {
				return substr($path, strlen($dir) + 1);
			}
		}
		return null;
	}
	
	public static function pad($str, $len, $ch = ' ')
	{
		if(strlen($str) > $len) {
			return substr($str, 0, $len - 3) . '...';
		}
		return $str . str_repeat($ch, $len - strlen($str));
	}
	
	protected function getFileModificationStamp($file)
	{
		if($this->ini['global']['fileModificationMethod'] == 'mtime') {
			return filemtime($file);
		}
		else if($this->ini['global']['fileModificationMethod'] == 'md5') {
			return md5_file($file);
		}
		else {
			throw new Exception("global.fileModificationMethod '{$this->ini['global']['fileModificationMethod']}' is invalid.");
		}
	}
	
	protected function fileHasChanged($file1, $file2)
	{
		if($this->ini['global']['fileModificationMethod'] == 'mtime') {
			return filemtime($file1) != $file2;
		}
		else if($this->ini['global']['fileModificationMethod'] == 'md5') {
			return md5_file($file1) != $file2;
		}
		else {
			throw new Exception("global.fileModificationMethod '{$this->ini['global']['fileModificationMethod']}' is invalid.");
		}
	}
	
	public function showErrors()
	{
		$results = $this->db->query("SELECT * FROM test WHERE errors != ''");
		while($row = $results->fetchArray(SQLITE3_ASSOC)) {
			echo $row['test_file'] . "\n\n";
			
			$files = $this->db->query("SELECT * FROM test_file WHERE test_file = '{$row['test_file']}' ORDER BY source_file");
			for($i = 1; $file = $files->fetchArray(SQLITE3_ASSOC); ++$i) {
				echo "  $i. ";
				$this->printColorText($file['source_file'] . "\n", Color::BLUE);
			}
			
			$this->printColorText($row['errors'], Color::RED);
		}
		
		$this->printFinalStats();
	}

	protected function cleanDeletedFiles()
	{
        $files = array();
        
		$query = $this->db->query("SELECT DISTINCT test_file FROM test");
        while($row = $query->fetchArray()) {
            if(!file_exists($row['test_file'])) {
                $this->db->query("DELETE FROM test WHERE test_file='{$row['test_file']}'");
                if(!in_array($row['test_file'], $files)) {
                    $files[] = $row['test_file'];
                }
            }
        }
        
		$query = $this->db->query("SELECT DISTINCT test_file FROM test_file");
        while($row = $query->fetchArray()) {
            if(!file_exists($row['test_file'])) {
                $this->db->query("DELETE FROM test_file WHERE test_file='{$row['test_file']}'");
                if(!in_array($row['test_file'], $files)) {
                    $files[] = $row['test_file'];
                }
            }
        }
        
		$query = $this->db->query("SELECT DISTINCT source_file FROM test_file");
        while($row = $query->fetchArray()) {
            if(!file_exists($row['source_file'])) {
                $this->db->query("DELETE FROM test_file WHERE source_file='{$row['source_file']}'");
                if(!in_array($row['source_file'], $files)) {
                    $files[] = $row['source_file'];
                }
            }
        }
        
        if(count($files)) {
            echo "These files have been removed since last run:\n";
            sort($files);
            foreach($files as $file) {
                echo "  $file\n";
            }
            echo "\n";
        }
    }

	protected function runSingleTest($path)
	{
		$filesModified = array();
		$filesUnmodified = array();
		
		// see if the test needs to be run
		$dbTest = $this->db->query("SELECT * FROM test WHERE test_file='$path'")->fetchArray();
		if(false !== $dbTest) {
			// if the test previously failed or there were no tests we always run again
			if($dbTest['total_errors'] > 0 || $dbTest['total_tests'] < 0) {
				$filesModified[] = $dbTest['test_file'];
			}
			// check the test file
			else if($this->fileHasChanged($dbTest['test_file'], $dbTest['modified'])) {
				$filesModified[] = $dbTest['test_file'];
			}
			else {
				$filesUnmodified[] = $dbTest['test_file'];
			}
			
			// compare the modified times of the source files
			$results = $this->db->query("SELECT * FROM test_file WHERE test_file='$path'");
			while($row = $results->fetchArray(SQLITE3_ASSOC)) {
				// determine if the file has changed
				if($this->fileHasChanged($row['source_file'], $row['modified'])) {
					$filesModified[] = $row['source_file'];
				}
				else {
					$filesUnmodified[] = $row['source_file'];
				}
			}
		}
		
		// don't run the test if no files have been modified
		if(count($filesModified) == 0 && count($filesUnmodified) > 0) {
			// hide skipped
			if($this->showSkipped) {
				echo self::pad($this->getRelativeTestPath($path), $this->width) . '| Skipped (' . count($filesUnmodified) . ' files unmodified)' . "\n";
			}
			return;
		}
		
		// run the tests
		$phpUnitResult = `{$this->ini['phpunit']['exe']} {$this->ini['phpunit']['options']} $path 2>&1`;
		$out = explode("\n", $phpUnitResult);
		++$this->totalFiles;
		
		// check for PHP Fatal
		if(preg_match('/PHP Fatal/', $phpUnitResult)) {
			die(self::printColorText($phpUnitResult, Color::RED));
		}
		
		// read the included files from the test
		$includedFiles = explode("\n", file_get_contents(sys_get_temp_dir() . "/included_files.txt"));
		foreach($includedFiles as $file) {
			if($this->isSourceFile($file)) {
				// register
				$this->db->exec("REPLACE INTO test_file (test_file, source_file, modified) VALUES ('$path', '$file', '" . $this->getFileModificationStamp($file) . "')");
			}
		}
		
		// extract stats
		$totalTests = 0;
		$totalAssertions = 0;
		$totalErrors = 0;
		$totalIncomplete = 0;
		$totalSkipped = 0;
		
		preg_match('/Tests: (\d+)/', $phpUnitResult, $matches);
		if($matches) {
			$totalTests += $matches[1];
		}
		
		preg_match('/Assertions: (\d+)/', $phpUnitResult, $matches);
		if($matches) {
			$totalAssertions += $matches[1];
		}
		
		preg_match('/Incomplete: (\d+)/', $phpUnitResult, $matches);
		if($matches) {
			$totalIncomplete += $matches[1];
		}
		
		preg_match('/Errors: (\d+)/', $phpUnitResult, $matches);
		if($matches) {
			$totalErrors += $matches[1];
		}
		
		preg_match('/Skipped: (\d+)/', $phpUnitResult, $matches);
		if($matches) {
			$totalSkipped += $matches[1];
		}
		
		preg_match('/Skipped: (\d+)/', $phpUnitResult, $matches);
		if($matches) {
			$totalSkipped += $matches[1];
		}
		
		preg_match('/(\d+) failures?/', $phpUnitResult, $matches);
		if($matches) {
			$totalErrors += $matches[1];
		}
		
		preg_match('/(\d+) assertions?/', $phpUnitResult, $matches);
		if($matches) {
			$totalAssertions += $matches[1];
		}
		
		preg_match('/(\d+) tests?/', $phpUnitResult, $matches);
		if($matches) {
			$totalTests += $matches[1];
		}
		
		$this->totalTests += $totalTests;
		$this->totalAssertions += $totalAssertions;
		$this->totalErrors += $totalErrors;
		$this->totalIncomplete += $totalIncomplete;
		$this->totalSkipped += $totalSkipped;
		
		// print stats
		echo self::pad($this->getRelativeTestPath($path), $this->width) . '| ';
		$this->printStats($totalTests, $totalAssertions, $totalIncomplete, $totalErrors, $totalSkipped);
		echo "\n";
		
		// extract errors
		$errors = "";
		if($totalErrors > 0) {
			for($start = 0; $start < count($out); ++$start) {
				if(preg_match('/There .* \d+ error/', $out[$start]) || preg_match('/There .* \d+ failure/', $out[$start])) {
					break;
				}
			}
			for($end = count($out) - 1; $end >= $start; --$end) {
				if(strpos($out[$end], 'FAILURES!') !== false) {
					break;
				}
			}
			for($i = $start + 1; $i < $end; ++$i) {
				$errors .= "  " . $out[$i] . "\n";
			}
			echo $this->printColorText($errors, Color::RED);
		}
		
		// register this test
		$this->db->exec("REPLACE INTO test (test_file, last_run, modified, total_tests, total_assertions, total_incomplete, total_errors, total_skipped, errors) VALUES ('$path', " . time() . ", '" . $this->getFileModificationStamp($path) . "', $totalTests, $totalAssertions, $totalIncomplete, $totalErrors, $totalSkipped, '" . str_replace("'", "''", $errors) . "')");
	}
	
	public static function startsWith($full, $start)
	{
		return substr($full, 0, strlen($start)) == $start;
	}
	
	protected function printStats($totalTests, $totalAssertions, $totalIncomplete, $totalErrors, $totalSkipped, $totalFiles = 0)
	{
		$color = Color::GREEN;
		$text = '';
		if($totalFiles > 0) {
			$text = $totalFiles . " files, ";
		}
		$text .= $totalTests . " tests";
		
		if($totalAssertions > 0) {
			$text .= ', ' . $totalAssertions . " assertions";
		}
		if($totalIncomplete > 0) {
			$text .= ', ' . $totalIncomplete . " incomplete";
			$color = Color::YELLOW;
		}
		if($totalSkipped > 0) {
			$text .= ', ' . $totalSkipped . " skipped";
			$color = Color::YELLOW;
		}
		if($totalErrors > 0 || $totalTests == 0) {
			$text .= ', ' . $totalErrors . " errors";
			$color = Color::RED;
		}
		
		self::printColorText($text, $color);
	}
	
	protected function printFinalStats()
	{
		echo self::pad('', $this->width - 4) . 'Run | ';
		$this->printStats($this->totalTests, $this->totalAssertions, $this->totalIncomplete, $this->totalErrors, $this->totalSkipped, $this->totalFiles);
		echo "\n";
		
		$stat = $this->db->query("SELECT count(*) as files, sum(total_tests) as tests, sum(total_assertions) as assertions, sum(total_incomplete) as incomplete, sum(total_errors) as errors, sum(total_skipped) as skipped FROM test")->fetchArray();
		echo self::pad('', $this->width - 6) . 'Total | ';
		$this->printStats($stat['tests'], $stat['assertions'], $stat['incomplete'], $stat['errors'], $stat['skipped'], $stat['files']);
		echo "\n";
	}
	
	public static function printColorText($text, $foreground)
	{
		echo "\033[{$foreground}m{$text}\033[0m";
	}
	
	public function clean()
	{
		unlink(__DIR__ . "/db.{$this->dbVersion}.sqlite3");
		echo "Database deleted.\n";
	}
	
	/*protected function runDoxygen()
	{
		echo "Running doxygen...\n";
		system("cd .. && {$this->ini['doxygen']['exe']} 2> Tests/Temp/doxygen.txt");
		
		echo "Processing doxygen results...\n";
		$this->db->exec("DELETE FROM doxygen");
		
		$handle = fopen("Temp/doxygen.txt", "r");
		if(!$handle) {
			die("Fatal: Could not open Temp/doxygen.txt\n");
		}
		
		$this->db->exec("BEGIN TRANSACTION");
		$file = "";
		$lineNumber = 0;
		$message = "";
		for($doxygen_id = 0; ($line = fgets($handle, 4096)) !== false; ++$doxygen_id) {
			$line = trim($line);
			
			if(preg_match('/^(.*):(\d+): (.*)$/', $line, $matches)) {
				if($doxygen_id > 0) {
					$sql = "REPLACE INTO doxygen (doxygen_id, file, line, message) VALUES ($doxygen_id, '$file', '$lineNumber', '" .
						str_replace(array("'", "\\", "\n"), array("''", "\\\\", "\\n"), $message) . "')";
					//echo "$sql\n";
					$this->db->exec($sql);
				}
				list(, $file, $lineNumber, $message) = $matches;
			}
			else {
				$message .= "\n$line";
			}
		}
		$this->db->exec("COMMIT TRANSACTION");
		fclose($handle);
		
		$totalDoxygenFiles = $this->db->query("SELECT count(DISTINCT file) FROM doxygen")->fetchArray();
		echo "  Doxygen messages: $doxygen_id\n";
		echo "  File documented incorrectly: {$totalDoxygenFiles[0]}\n";
		echo "Done\n\n";
		
		// print doxygen messages
		$files = $this->db->query("SELECT file FROM doxygen GROUP BY file ORDER BY modified_time DESC LIMIT 3");
		while($fileRow = $files->fetchArray(SQLITE3_ASSOC)) {
			echo $this->printColorText("{$fileRow['file']}\n", Color::BLUE);
			
			$messages = $this->db->query("SELECT * FROM doxygen WHERE file='{$fileRow['file']}' ORDER BY line");
			for($messageCount = 1; $messageRow = $messages->fetchArray(SQLITE3_ASSOC); ++$messageCount) {
				echo "  $messageCount. ";
				echo $this->printColorText("line {$messageRow['line']}: {$messageRow['message']}\n", Color::BLUE);
				
				if($messageCount >= 10) {
					while($messages->fetchArray(SQLITE3_ASSOC)) {
						++$messageCount;
					}
					echo "  ... $messageCount total.\n";
					break;
				}
			}
			echo "\n";
		}
	}*/
	
}
