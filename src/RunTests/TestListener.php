<?php

namespace RunTests;

require_once('vendor/autoload.php');

class TestListener implements \PHPUnit_Framework_TestListener
{

    public function startTest(\PHPUnit_Framework_Test $test)
    {
    }
 
    public function endTest(\PHPUnit_Framework_Test $test, $time)
    {
        $path = sys_get_temp_dir() . "/included_files.txt";
        file_put_contents($path, implode("\n", get_included_files()));
    }

    public function addError(\PHPUnit_Framework_Test $test, \Exception $e, $time)
    {
    }

    public function addFailure(\PHPUnit_Framework_Test $test, \PHPUnit_Framework_AssertionFailedError $e, $time)
    {
    }

    public function addIncompleteTest(\PHPUnit_Framework_Test $test, \Exception $e, $time)
    {
    }

    public function addSkippedTest(\PHPUnit_Framework_Test $test, \Exception $e, $time)
    {
    }
    
    public function startTestSuite(\PHPUnit_Framework_TestSuite $suite)
    {
    }
    
    public function endTestSuite(\PHPUnit_Framework_TestSuite $suite)
    {
    }

    public function addRiskyTest(\PHPUnit_Framework_Test $test, \Exception $e, $time)
    {
    }

}
