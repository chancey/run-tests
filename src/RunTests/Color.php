<?php

namespace RunTests;

require_once('vendor/autoload.php');

class Color
{

	const GREEN = '0;32';
	
	const RED = '0;31';
	
	const YELLOW = '1;33';
	
	const BLUE = '1;34';

}
