<?php

require_once('vendor/autoload.php');

$runTests = new RunTests\RunTests();
if($argc < 2) {
	$argv[1] = "run";
}

if($argv[1] == "clean") {
	$runTests->clean();
}
else if($argv[1] == "run") {
	$runTests->run();
}
else {
	$runTests->showErrors();
}
